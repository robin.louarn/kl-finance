import { Contact } from "@/components/contact";
import { Faq } from "@/components/faq";
import { Hero } from "@/components/hero";
import { Needs } from "@/components/needs";
import { Services } from "@/components/services";
import { Testimonial } from "@/components/testimonial";
import { FC } from "react";

const Home: FC = () => (
  <main className="flex flex-col items-center gap-5">
    <Hero />
    <Services />
    <Needs />
    <Testimonial />
    <Contact />
    <Faq />
  </main>
);

export default Home;
