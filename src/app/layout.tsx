import type { Metadata } from "next";
import { Geist, Geist_Mono } from "next/font/google";
import "./globals.css";
import { FC, PropsWithChildren } from "react";
import { Navbar } from "@/components/nav-bar";
import { Footer } from "@/components/footer";

const geistSans = Geist({
  variable: "--font-geist-sans",
  subsets: ["latin"],
});

const geistMono = Geist_Mono({
  variable: "--font-geist-mono",
  subsets: ["latin"],
});

export const metadata: Metadata = {
  title: "KL Finance",
  description: "Auto-entrepreneur expert en regroupement de crédit",
};

const RootLayout: FC<PropsWithChildren> = ({ children }) => (
  <html lang="fr" className="scroll-smooth">
    <body className={`${geistSans.variable} ${geistMono.variable} antialiased`}>
      <Navbar />
      {children}
      <Footer />
    </body>
  </html>
);

export default RootLayout;
