export const h1 =
  "scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl";
export const lead = "text-xl text-muted-foreground";
export const large = "text-lg font-semibold";
