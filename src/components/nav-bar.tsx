import Link from "next/link";
import { FC } from "react";
import { buttonVariants } from "./ui/button";

export const Navbar: FC = () => (
  <ul className="flex flex-col items-center md:flex-row md:justify-center md:gap-10 py-5">
    <li>
      <Link href="#hero" className={buttonVariants({ variant: "link" })}>
        KL Finance
      </Link>
    </li>
    <li>
      <Link href="#services" className={buttonVariants({ variant: "link" })}>
        Services
      </Link>
    </li>
    <li>
      <Link href="#needs" className={buttonVariants({ variant: "link" })}>
        Needs
      </Link>
    </li>
    <li>
      <Link href="#contact" className={buttonVariants({ variant: "link" })}>
        Contact
      </Link>
    </li>
    <li>
      <Link href="#faq" className={buttonVariants({ variant: "link" })}>
        FAQ
      </Link>
    </li>
  </ul>
);
