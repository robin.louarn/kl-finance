import { PiggyBankIcon, MinusCircleIcon, RocketIcon } from "lucide-react";
import { FC } from "react";

export const Needs: FC = () => (
  <section
    id="needs"
    className="bg-muted flex flex-col items-center gap-12 w-full"
  >
    <div className="flex">
      <h2 className="text-3xl font-bold lg:text-4xl">
        Un financement adapté à vos besoins et aspirations
      </h2>
    </div>
    <div className="space-y-6 lg:space-y-10">
      <div className="flex">
        <MinusCircleIcon className="flex-shrink-0 mt-2 h-6 w-6" />
        <div className="ms-5 sm:ms-8">
          <h3 className="text-base sm:text-lg font-semibold">
            Diminuer vos mensualités
          </h3>
          <p className="mt-1 text-muted-foreground">
            Réduisez vos charges et simplifiez la gestion de vos finances.
          </p>
        </div>
      </div>
      <div className="flex">
        <RocketIcon className="flex-shrink-0 mt-2 h-6 w-6" />
        <div className="ms-5 sm:ms-8">
          <h3 className="text-base sm:text-lg font-semibold">
            Financer un projet
          </h3>
          <p className="mt-1 text-muted-foreground">
            Donnez vie à vos idées avec un financement sur-mesure.
          </p>
        </div>
      </div>
      <div className="flex">
        <PiggyBankIcon className="flex-shrink-0 mt-2 h-6 w-6" />
        <div className="ms-5 sm:ms-8">
          <h3 className="text-base sm:text-lg font-semibold">
            Constituer un placement
          </h3>
          <p className="mt-1 text-muted-foreground">
            Sécurisez votre avenir avec une épargne adaptée à vos ambitions.
          </p>
        </div>
      </div>
    </div>
  </section>
);
