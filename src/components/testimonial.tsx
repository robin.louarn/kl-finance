import Image from "next/image";
import { FC } from "react";

export const Testimonial: FC = () => (
  <section>
    <div className="container mt-16 sm:mt-0">
      <div className="w-full columns-1 gap-4 sm:columns-2 lg:columns-3 lg:gap-6 [&amp;>div:nth-child(n+5)]:hidden sm:[&amp;>div:nth-child(n+5)]:inline-block sm:[&amp;>div:nth-child(n+9)]:hidden lg:[&amp;>div:nth-child(n+9)]:inline-block">
        <div className="mb-4 inline-block w-full rounded-lg border border-border p-6 lg:mb-6">
          <div className="flex flex-col">
            <p className="mb-4 text-xs">
              &quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Pellentesque tincidunt urna ac tortor molestie, sit amet lobortis
              massa cursus. Ut rutrum nunc sit amet tellus cursus congue.&quot;
            </p>
            <div className="flex items-center gap-1 md:gap-2">
              <span className="relative flex shrink-0 overflow-hidden rounded-full size-8 md:size-10">
                <Image
                  className="aspect-square h-full w-full"
                  src="https://shadcnblocks.com/images/block/avatar-1.webp"
                  width="288"
                  height="288"
                  alt="avatar"
                />
              </span>
              <div className="text-left">
                <p className="text-xs font-medium">Customer Name</p>
                <p className="text-xs ">Company Name</p>
              </div>
            </div>
          </div>
        </div>
        <div className="mb-4 inline-block w-full rounded-lg border border-border bg-background p-6 lg:mb-6">
          <div className="flex flex-col">
            <p className="mb-4 text-xs">
              &quot;Sed sodales ligula non neque molestie.&quot;
            </p>
            <div className="flex items-center gap-1 md:gap-2">
              <span className="relative flex shrink-0 overflow-hidden rounded-full size-8 md:size-10">
                <Image
                  className="aspect-square h-full w-full"
                  src="https://shadcnblocks.com/images/block/avatar-1.webp"
                  width="288"
                  height="288"
                  alt="avatar"
                />
              </span>
              <div className="text-left">
                <p className="text-xs font-medium">Customer Name</p>
                <p className="text-xs ">Company Name</p>
              </div>
            </div>
          </div>
        </div>
        <div className="mb-4 inline-block w-full rounded-lg border border-border bg-background p-6 lg:mb-6">
          <div className="flex flex-col">
            <p className="mb-4 text-xs">
              &quot;Sed sodales ligula non neque molestie, et auctor quam
              fringilla. Donec placerat justo et vehicula interdum.&quot;
            </p>
            <div className="flex items-center gap-1 md:gap-2">
              <span className="relative flex shrink-0 overflow-hidden rounded-full size-8 md:size-10">
                <Image
                  className="aspect-square h-full w-full"
                  src="https://shadcnblocks.com/images/block/avatar-1.webp"
                  width="288"
                  height="288"
                  alt="avatar"
                />
              </span>
              <div className="text-left">
                <p className="text-xs font-medium">Customer Name</p>
                <p className="text-xs ">Company Name</p>
              </div>
            </div>
          </div>
        </div>
        <div className="mb-4 inline-block w-full rounded-lg border border-border bg-background p-6 lg:mb-6">
          <div className="flex flex-col">
            <p className="mb-4 text-xs">
              &quot;Lorem ipsum dolor sit amet, consectetur adipiscing
              elit.&quot;
            </p>
            <div className="flex items-center gap-1 md:gap-2">
              <span className="relative flex shrink-0 overflow-hidden rounded-full size-8 md:size-10">
                <Image
                  className="aspect-square h-full w-full"
                  src="https://shadcnblocks.com/images/block/avatar-1.webp"
                  width="288"
                  height="288"
                  alt="avatar"
                />
              </span>
              <div className="text-left">
                <p className="text-xs font-medium">Customer Name</p>
                <p className="text-xs ">Company Name</p>
              </div>
            </div>
          </div>
        </div>
        <div className="mb-4 inline-block w-full rounded-lg border border-border bg-background p-6 lg:mb-6">
          <div className="flex flex-col">
            <p className="mb-4 text-xs">
              &quot;Lorem ipsum dolor sit amet, consectetur adipiscing
              elit.&quot;
            </p>
            <div className="flex items-center gap-1 md:gap-2">
              <span className="relative flex shrink-0 overflow-hidden rounded-full size-8 md:size-10">
                <Image
                  className="aspect-square h-full w-full"
                  src="https://shadcnblocks.com/images/block/avatar-1.webp"
                  width="288"
                  height="288"
                  alt="avatar"
                />
              </span>
              <div className="text-left">
                <p className="text-xs font-medium">Customer Name</p>
                <p className="text-xs ">Company Name</p>
              </div>
            </div>
          </div>
        </div>
        <div className="mb-4 inline-block w-full rounded-lg border border-border bg-background p-6 lg:mb-6">
          <div className="flex flex-col">
            <p className="mb-4 text-xs">
              &quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Pellentesque tincidunt urna ac tortor molestie, sit amet lobortis
              massa cursus. Ut rutrum nunc sit amet tellus cursus congue.&quot;
            </p>
            <div className="flex items-center gap-1 md:gap-2">
              <span className="relative flex shrink-0 overflow-hidden rounded-full size-8 md:size-10">
                <Image
                  className="aspect-square h-full w-full"
                  src="https://shadcnblocks.com/images/block/avatar-1.webp"
                  width="288"
                  height="288"
                  alt="avatar"
                />
              </span>
              <div className="text-left">
                <p className="text-xs font-medium">Customer Name</p>
                <p className="text-xs ">Company Name</p>
              </div>
            </div>
          </div>
        </div>
        <div className="mb-4 inline-block w-full rounded-lg border border-border bg-background p-6 lg:mb-6">
          <div className="flex flex-col">
            <p className="mb-4 text-xs">
              &quot;Sed sodales ligula non neque molestie, et auctor quam
              fringilla. Donec placerat justo et vehicula interdum.&quot;
            </p>
            <div className="flex items-center gap-1 md:gap-2">
              <span className="relative flex shrink-0 overflow-hidden rounded-full size-8 md:size-10">
                <Image
                  className="aspect-square h-full w-full"
                  src="https://shadcnblocks.com/images/block/avatar-1.webp"
                  width="288"
                  height="288"
                  alt="avatar"
                />
              </span>
              <div className="text-left">
                <p className="text-xs font-medium">Customer Name</p>
                <p className="text-xs ">Company Name</p>
              </div>
            </div>
          </div>
        </div>
        <div className="mb-4 inline-block w-full rounded-lg border border-border bg-background p-6 lg:mb-6">
          <div className="flex flex-col">
            <p className="mb-4 text-xs">
              &quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Pellentesque tincidunt urna ac tortor molestie, sit amet lobortis
              massa cursus. Ut rutrum nunc sit amet tellus cursus congue.&quot;
            </p>
            <div className="flex items-center gap-1 md:gap-2">
              <span className="relative flex shrink-0 overflow-hidden rounded-full size-8 md:size-10">
                <Image
                  className="aspect-square h-full w-full"
                  src="https://shadcnblocks.com/images/block/avatar-1.webp"
                  width="288"
                  height="288"
                  alt="avatar"
                />
              </span>
              <div className="text-left">
                <p className="text-xs font-medium">Customer Name</p>
                <p className="text-xs ">Company Name</p>
              </div>
            </div>
          </div>
        </div>
        <div className="mb-4 inline-block w-full rounded-lg border border-border bg-background p-6 lg:mb-6">
          <div className="flex flex-col">
            <p className="mb-4 text-xs">
              &quot;Lorem ipsum dolor sit amet, consectetur adipiscing
              elit.&quot;
            </p>
            <div className="flex items-center gap-1 md:gap-2">
              <span className="relative flex shrink-0 overflow-hidden rounded-full size-8 md:size-10">
                <Image
                  className="aspect-square h-full w-full"
                  src="https://shadcnblocks.com/images/block/avatar-1.webp"
                  width="288"
                  height="288"
                  alt="avatar"
                />
              </span>
              <div className="text-left">
                <p className="text-xs font-medium">Customer Name</p>
                <p className="text-xs ">Company Name</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
);
