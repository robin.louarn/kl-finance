import { FC } from "react";
import { Label } from "./ui/label";
import { Input } from "./ui/input";
import { Button, buttonVariants } from "./ui/button";
import { Textarea } from "./ui/textarea";
import { Mail, MapPin, Phone } from "lucide-react";
import Link from "next/link";

const contacts = [
  {
    Icon: Mail,
    title: "Email Us",
    description: "Our team is ready to assist.",
    linkText: "klouarn@icloud.com",
    href: "mailto:klouarn@icloud.com",
  },
  {
    Icon: MapPin,
    title: "Visit Us",
    description: "Drop by our office for a chat.",
    linkText: "Saint-Brieuc, Bretagne",
    href: "#",
  },
  {
    Icon: Phone,
    title: "Call Us",
    description: "We're available Mon-Fri, 9am-5pm.",
    linkText: "06 60 61 15 23",
    href: "tel:0660611523",
  },
];

export const Contact: FC = () => (
  <section
    id="contact"
    className="magicpattern w-full px-10 lg:py-32 mx-auto flex flex-col gap-10 lg:flex-row lg:gap-20"
  >
    <div className="grid gap-10 sm:grid-cols-2">
      {contacts.map(({ Icon, title, description, linkText, href }, idx) => (
        <div key={idx} className="flex flex-col items-start">
          <div className="flex gap-3">
            <Icon />
            <p>{title}</p>
          </div>
          <p>{description}</p>
          <Link
            href={href}
            className={buttonVariants({ variant: "link", className: "px-0" })}
          >
            {linkText}
          </Link>
        </div>
      ))}
    </div>

    <div className="mx-auto flex max-w-screen-md flex-col gap-6 rounded-lg border p-10">
      <div className="flex gap-4">
        <div className="grid w-full items-center gap-1.5">
          <Label htmlFor="firstname">First Name</Label>
          <Input type="text" id="firstname" placeholder="First Name" />
        </div>
        <div className="grid w-full items-center gap-1.5">
          <Label htmlFor="lastname">Last Name</Label>
          <Input type="text" id="lastname" placeholder="Last Name" />
        </div>
      </div>
      <div className="grid w-full items-center gap-1.5">
        <Label htmlFor="email">Email</Label>
        <Input type="email" id="email" placeholder="Email" />
      </div>
      <div className="grid w-full items-center gap-1.5">
        <Label htmlFor="subject">Subject</Label>
        <Input type="text" id="subject" placeholder="Subject" />
      </div>
      <div className="grid w-full gap-1.5">
        <Label htmlFor="message">Message</Label>
        <Textarea placeholder="Type your message here." id="message" />
      </div>
      <Button className="w-full">Send Message</Button>
    </div>
  </section>
);
