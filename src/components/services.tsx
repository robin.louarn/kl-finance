import {
  HomeIcon,
  UserCheckIcon,
  GlobeIcon,
  CalendarCheckIcon,
} from "lucide-react";
import { FC } from "react";

export const Services: FC = () => (
  <section id="services" className="px-5 lg:py-32">
    <div className="grid sm:grid-cols-2 lg:grid-cols-4 items-center gap-12">
      <div className="text-center">
        <div className="flex justify-center items-center w-12 h-12 bg-primary border rounded-full mx-auto">
          <UserCheckIcon className="flex-shrink-0 w-5 h-5 text-primary-foreground" />
        </div>
        <div className="mt-3">
          <h3 className="text-lg font-semibold ">
            Un accompagnement de qualité
          </h3>
          <p className="mt-1 text-muted-foreground">
            Bénéficiez d’un suivi personnalisé par un expert.
          </p>
        </div>
      </div>
      <div className="text-center">
        <div className="flex justify-center items-center w-12 h-12 bg-primary border rounded-full mx-auto">
          <HomeIcon className="flex-shrink-0 w-5 h-5 text-primary-foreground" />
        </div>
        <div className="mt-3">
          <h3 className="text-lg font-semibold ">Un seul interlocuteur</h3>
          <p className="mt-1 text-muted-foreground">
            Un conseiller unique pour une gestion simplifiée de vos besoins.
          </p>
        </div>
      </div>
      <div className="text-center">
        <div className="flex justify-center items-center w-12 h-12 bg-primary border rounded-full mx-auto">
          <GlobeIcon className="flex-shrink-0 w-5 h-5 text-primary-foreground" />
        </div>
        <div className="mt-3">
          <h3 className="text-lg font-semibold ">Une proposition surmesure</h3>
          <p className="mt-1 text-muted-foreground">
            Profitez d’un large éventail de solutions adaptées à vos besoins.
          </p>
        </div>
      </div>
      <div className="text-center">
        <div className="flex justify-center items-center w-12 h-12 bg-primary border rounded-full mx-auto">
          <CalendarCheckIcon className="flex-shrink-0 w-5 h-5 text-primary-foreground" />
        </div>
        <div className="mt-3">
          <h3 className="text-lg font-semibold ">Un rendez-vous rapide</h3>
          <p className="mt-1 text-muted-foreground">
            Organisez un rendez-vous chez vous en toute simplicité.
          </p>
        </div>
      </div>
    </div>
  </section>
);
