import { FC } from "react";
import { Button } from "./ui/button";

export const AskSimulation: FC = () => <Button>Demander une simulation</Button>;
